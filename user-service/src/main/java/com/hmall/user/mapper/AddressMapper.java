package com.hmall.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmall.common.pojo.Address;

public interface AddressMapper extends BaseMapper<Address> {

}