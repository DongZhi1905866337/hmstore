package com.hmall.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;

@MapperScan("com.hmall.user.mapper")
@SpringBootApplication
//@EnableFeignClients(basePackages = "com.hmall.client")
public class UserApplication {
    @LoadBalanced
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}
