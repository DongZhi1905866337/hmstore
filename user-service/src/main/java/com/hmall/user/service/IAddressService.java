package com.hmall.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.common.pojo.Address;

public interface IAddressService extends IService<Address> {
}
