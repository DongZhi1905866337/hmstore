package com.hmall;

import com.alibaba.fastjson.JSON;
import com.hmall.client.ItemClient;
import com.hmall.constants.HmallConstants;
import com.hmall.dto.PageDTO;
import com.hmall.dto.SearchParams;
import com.hmall.pojo.Item;
import com.hmall.pojo.ItemDOC;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;


/*
 *@author: RickChen
 *@description:
 *@Time: 2023/4/9  10:24
 */
@SpringBootTest
public class SearchTest {
    @Resource
    private RestHighLevelClient restHighLevelClient;


    @Resource
    ItemClient itemClient;

    /**
     * 创建索引库
     *
     * @throws IOException
     */
    @Test
    public void createIndex() throws IOException {
        // 1.创建 创建索引请求对象并传入索引名
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("hamll");
        // 2. 传入实际执行json及其传参类型
        createIndexRequest.source(HmallConstants.Hmall_MAPPING_TEMPLATE, XContentType.JSON);

        // 执行创建
        restHighLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);

    }


    /**
     * 远程调用Item 大量数据批量导入
     *
     * @throws IOException
     */
    @Test
    public void test04() throws IOException {
        // 1.从mysql获取批量数据

        int count = (itemClient.count()) / 1000;
        // 2.创建批量添加doc对象，并添加全局index名称
        BulkRequest bulkRequest = new BulkRequest("hmall");
        PageDTO page;
        SearchParams searchParams = new SearchParams();
        List list;
        for (int i = 1; i <= count + 1; i++) {
            searchParams.setPage(i);
            searchParams.setSize(1000);
            page = itemClient.list(i, 1000);
            list = page.getList();

            // 3.遍历原始数据集合
            for (Object item : list) {

                // 3.1转换为Doc
                ItemDOC itemDOC = new ItemDOC((Item) item);
                // 3.2开始进行添加 ，创建出单个对象并填充对象数据传入
                bulkRequest.add(
                        new IndexRequest().id(itemDOC.getId().toString())
                                .source(JSON.toJSONString(itemDOC), XContentType.JSON)
                );
            }
            // 4.批量添加请求对象准备完毕，进行添加
            restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            bulkRequest.requests().clear();// 清理bulk
        }

    }

}
