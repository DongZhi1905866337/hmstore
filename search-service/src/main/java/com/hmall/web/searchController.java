package com.hmall.web;

import com.hmall.dto.PageDTO;
import com.hmall.pojo.ItemDOC;
import com.hmall.service.SearchService;
import com.hmall.web.request.SearchParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/*
 *@author: RickChen
 *@description: 分布式搜索功能入口
 *@Time: 2023/4/9  14:30
 */
@RestController
@RequestMapping("/search")
@Slf4j
public class searchController {

    @Resource
    private SearchService searchService;

    @PostMapping("/filters")
    public Map<String, List<String>> filters(@RequestBody SearchParams searchParams) {
        log.info("查询参数:{}", searchParams);
        return searchService.fileters(searchParams);
    }


    @GetMapping("/suggestion")
    public List<String> suggestion(String key) {
        log.info("补全参数:{}", key);
        return searchService.suggestion(key);
    }

    @PostMapping("/list")
    public PageDTO<ItemDOC> list(@RequestBody SearchParams searchParams) {
        log.info("查询参数:{}", searchParams);
        return searchService.list(searchParams);
    }


}
