package com.hmall.service;

import com.hmall.dto.PageDTO;
import com.hmall.pojo.ItemDOC;
import com.hmall.web.request.SearchParams;

import java.util.List;
import java.util.Map;


public interface SearchService {
    Map<String, List<String>> fileters(SearchParams searchParams);


    List<String> suggestion(String key);


    PageDTO<ItemDOC> list(SearchParams searchParams);

}
