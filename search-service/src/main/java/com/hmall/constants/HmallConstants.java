package com.hmall.constants;

/*
 *@author: RickChen
 *@description: 酒店常量值类
 *@Time: 2023/4/4  16:59
 */
public class HmallConstants {
public final static String Hmall_MAPPING_TEMPLATE= "\"settings\": {\n" +
        "    \"analysis\": {\n" +
        "      \"analyzer\": {\n" +
        "        \"text_anlyzer\": {\n" +
        "          \"tokenizer\": \"ik_max_word\",\n" +
        "          \"filter\": \"py\"\n" +
        "        },\n" +
        "        \"completion_analyzer\": {\n" +
        "          \"tokenizer\": \"keyword\",\n" +
        "          \"filter\": \"py\"\n" +
        "        }\n" +
        "      },\n" +
        "      \"filter\": {\n" +
        "        \"py\": {\n" +
        "          \"type\": \"pinyin\",\n" +
        "          \"keep_full_pinyin\": false,\n" +
        "          \"keep_joined_full_pinyin\": true,\n" +
        "          \"keep_original\": true,\n" +
        "          \"limit_first_letter_length\": 16,\n" +
        "          \"remove_duplicated_term\": true,\n" +
        "          \"none_chinese_pinyin_tokenize\": false\n" +
        "        }\n" +
        "      }\n" +
        "    }\n" +
        "  },\n" +
        "  \"mappings\": {\n" +
        "    \"properties\": {\n" +
        "      \"id\":{\n" +
        "        \"type\": \"keyword\"\n" +
        "      },\n" +
        "      \"name\":{\n" +
        "        \"type\": \"text\",\n" +
        "        \"analyzer\": \"text_anlyzer\",\n" +
        "        \"search_analyzer\": \"ik_smart\"\n" +
        "        , \"copy_to\": \"all\"\n" +
        "      },\n" +
        "      \"price\":{\n" +
        "        \"type\": \"integer\"\n" +
        "      },\n" +
        "       \"stock\":{\n" +
        "        \"type\": \"integer\"\n" +
        "      },\n" +
        "      \"image\":{\n" +
        "        \"type\": \"keyword\",\n" +
        "        \"index\": false\n" +
        "      },\n" +
        "       \"category\":{\n" +
        "        \"type\": \"keyword\"\n" +
        "          , \"copy_to\": \"all\"\n" +
        "\n" +
        "      },\n" +
        "       \"brand\":{\n" +
        "        \"type\": \"keyword\"\n" +
        "          , \"copy_to\": \"all\"\n" +
        "  \n" +
        "      },\n" +
        "       \"spec\":{\n" +
        "        \"type\": \"keyword\"\n" +
        "      },\n" +
        "      \"sold\":{\n" +
        "        \"type\": \"integer\"\n" +
        "      },\n" +
        "      \"commentCount\":{\n" +
        "        \"type\": \"integer\"\n" +
        "      },\n" +
        "       \"isAD\":{\n" +
        "        \"type\": \"boolean\"\n" +
        "      },\n" +
        "        \"status\":{\n" +
        "        \"type\": \"integer\"\n" +
        "      },\n" +
        "       \"createTime\":{\n" +
        "        \"type\": \"date\"\n" +
        "      },\n" +
        "       \"updateTime\":{\n" +
        "        \"type\": \"date\"\n" +
        "      },\n" +
        "      \"all\":{\n" +
        "        \"type\": \"text\",\n" +
        "        \"analyzer\": \"text_anlyzer\",\n" +
        "        \"search_analyzer\": \"ik_smart\"\n" +
        "      },\n" +
        "       \"suggest\":{\n" +
        "        \"type\": \"completion\",\n" +
        "        \"analyzer\": \"pinyin\"\n" +
        "      }\n" +
        "    }\n" +
        "  }";

}
