package com.hmall;

import com.hmall.client.ItemClient;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableFeignClients(basePackageClasses = ItemClient.class)
public class SearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(SearchApplication.class, args);
    }

     // 初始化Es客户端
    @Bean
    public RestHighLevelClient initES(){
        return new RestHighLevelClient(RestClient.builder(HttpHost.create("http://192.168.40.200:9200")));
    }

}
