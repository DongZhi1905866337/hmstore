package com.hmall.interceptor;


import com.hmall.context.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * jwt令牌校验的拦截器
 */
@Component
    //TODO 远程调用解耦合
@Slf4j
public class AuthorizationInterceptor implements HandlerInterceptor {


   /* @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Long authorization = Long.valueOf(request.getHeader("authorization"));
        BaseContext.setCurrentId(authorization);
        return true;
    }*/
}
