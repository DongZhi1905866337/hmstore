package com.hmall.order;

import com.hmall.client.AddressClient;
import com.hmall.client.ItemClient;
import com.hmall.client.OrderClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;

@MapperScan("com.hmall.order.mapper")
@SpringBootApplication
@EnableFeignClients(basePackageClasses = {ItemClient.class, AddressClient.class, OrderClient.class})
public class OrderApplication {

    @LoadBalanced
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

}