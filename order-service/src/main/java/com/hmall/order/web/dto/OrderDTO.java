package com.hmall.order.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 接收前端用户下订单数据信息
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {

    //num代表购买数量
    private Integer num;

    //paymentType付款方式
    private Integer paymentType;

    //addressId代表收货人地址id
    private Integer addressId;

    //itemId代表商品id
    private Long itemId;
}
