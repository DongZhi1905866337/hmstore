package com.hmall.order.web;

import com.hmall.common.pojo.Order;
import com.hmall.order.service.IOrderService;
import com.hmall.order.web.dto.OrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    /**
     * 根据id返回对象
     *
     * @param orderId
     * @return
     */
    @GetMapping("/{id}")
    public Order findById(@PathVariable("id") String orderId) {

        return orderService.getById(Long.valueOf(orderId.substring(1, orderId.length())));
    }

    /**
     * 用户下单创建订单并扣减库存
     *
     * @param orderDTO 前端传的参数
     * @return 订单id
     */
    @PostMapping
    public String createOrder(@RequestBody OrderDTO orderDTO) {
        Long orderId = orderService.createOrder(orderDTO);
        return "a" + orderId;
    }
}
