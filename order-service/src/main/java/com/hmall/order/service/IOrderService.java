package com.hmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.common.pojo.Order;
import com.hmall.order.web.dto.OrderDTO;

public interface IOrderService extends IService<Order> {
    /**
     * 前端用户传的订单数据参数
     * @param orderDTO
     * @return
     */
    Long createOrder(OrderDTO orderDTO);
}
