package com.hmall.item.web.request;

import lombok.Data;

/*
 *@author: RickChen
 *@description: 查询请求参数类
 *@Time: 2023/4/9  14:34
 */
@Data
public class SearchParams {
private String key;
private Integer page;
private String sortBy;
private String category;
private String brand;
private Integer minPrice;
private Integer maxPrice;
private Integer size;
}
