package com.hmall.item.web.request;

import lombok.Data;

@Data
public class ItemPageQuery {

    /**
     * 当前请求页码
     */
    private Integer page;

    /**
     * 请求页大小
     */
    private Integer size;
}
