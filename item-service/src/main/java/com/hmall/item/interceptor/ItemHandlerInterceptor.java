package com.hmall.item.interceptor;

import com.hmall.common.context.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class ItemHandlerInterceptor  implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断当前拦截到的是Controller的方法还是其他资源
        if (!(handler instanceof HandlerMethod)) {
            //当前拦截到的不是动态方法，直接放行
            return true;
        }
            //获取请求头的用户id
            Long userId = Long.valueOf(request.getHeader("authorization"));
            //
            if (userId==null){
                throw new RuntimeException();
            }
            //将用户id存储到ThreadLocal
            BaseContext.setCurrentId(userId);

            //如果有登录数据，代表一登录，放行
            return true;
    }
}
