package com.hmall.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmall.item.mapper.ItemMapper;
import com.hmall.item.service.IItemService;
import com.hmall.pojo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ItemService extends ServiceImpl<ItemMapper, Item> implements IItemService {
    @Autowired
    private ItemMapper itemMapper;
    /**
     * 更新库存
     *
     * @param itemId
     * @param num
     * @return
     */
    @Override
    public Integer updateByNum(Long itemId, Integer num) {
        Item item = itemMapper.selectById(itemId);
        item.setStock(item.getStock()-num);
        item.setUpdateTime(new Date());
        int count = itemMapper.updateById(item);
        return count;
    }
}
