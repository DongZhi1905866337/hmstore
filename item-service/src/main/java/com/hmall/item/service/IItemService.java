package com.hmall.item.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hmall.pojo.Item;

public interface IItemService extends IService<Item> {
    /**
     * 更新库存
     * @param itemId
     * @param num
     * @return
     */
    Integer updateByNum(Long itemId, Integer num);
}
