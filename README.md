# hmstore

#### 介绍
你有heima store吗？

#### 软件架构
你说的对，但是《黑马商城》是由13组自主研发的一款全新开放购物系统。系统部署在一个被称作「windows」的操作系统，在这里，被神选中的开发将被授予「git分支」，导引分布式之力。你将扮演一位名为「程序猿」的神秘角色

在自由的旅行中邂逅性格各异、能力独特的码农们，和他们一起击败bug，找回失散的微服务——同时，逐步发掘「java」的真相。


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
