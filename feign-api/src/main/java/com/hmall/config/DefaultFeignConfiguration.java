package com.hmall.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;

/*
 *@author: RickChen
 *@description:
 *@Time: 2023/3/28  18:09
 */
public class DefaultFeignConfiguration {
    @Bean
    public Logger.Level feignLogLevel(){
        return Logger.Level.BASIC;    // 配置Feign为Basic等级日志
    }
}
