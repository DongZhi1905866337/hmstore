package com.hmall.client;

import com.hmall.common.pojo.Address;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@FeignClient(value = "userservice")
@RestController
public interface AddressClient {
    @GetMapping("/address/{id}")
    Address findAddressById(@PathVariable("id") Integer id);
}
