package com.hmall.client;

import com.hmall.dto.PageDTO;
import com.hmall.dto.SearchParams;
import com.hmall.pojo.Item;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/*
 *@author: RickChen
 *@description:
 *@Time: 2023/3/28  17:47
 */
@FeignClient("itemservice")
@RestController
public interface ItemClient {

    @GetMapping("/item/list")
    public PageDTO<Item> list( @RequestParam Integer page ,@RequestParam Integer size);


    @GetMapping("/item/{id}")
    public Item findById(@PathVariable Long id) ;


    @PostMapping()
    public void add(@RequestBody Item item);

    @PutMapping("/item/status/{id}/{status}")
    public void status(@PathVariable String id, @PathVariable Integer status) ;

    @PutMapping()
    public void update(@RequestBody Item item) ;

    @DeleteMapping("/item/{id}")
    public void delete(@PathVariable String id) ;

    @GetMapping("/item/count")
    public Integer count();


    @PutMapping("/item/stock/{itemId}/{num}")
    Integer updateByNum(@PathVariable("itemId")Long itemId,
                        @PathVariable("num")Integer num);
}
